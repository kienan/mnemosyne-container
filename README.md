# Mnemosyne sync server container

Note: the image is quite large since mnemosyne (by default) requires much of Qt5 including the graphics stack.

Usage:

  * Start with `podman-compose up`
  * Once started, add the following to the config.py file in the data volume:
```
remote_access_username = "user"
remote_access_password = "password"
remote_access_password_algo = ""
run_sync_server = True
```
  * Restart the container twice. On the first run, it seems to create the config.db
  with empty values only. On the second run, the values are properly set in the db
  and upgraded to being hashed using argon2id.
  * The lines matching "remote_access_*" may be removed from config.py and the container
  restart for a final time.
